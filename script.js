let buttons = document.querySelectorAll(".tabs-title");
let tabs = document.querySelector(".tabs");

tabs.addEventListener("click", (event) => {
  if (event.target.classList.contains("tabs-title")) {
    document.querySelector(".active.tab__item").classList.remove(`active`);
    document.querySelector(".active.tabs-title").classList.remove(`active`);
    document
      .querySelector(event.target.getAttribute("data-tab"))
      .classList.add(`active`);
    event.target.classList.add(`active`);
  }
});
let loadMoreBtn = document.querySelector(".grid-button");
loadMoreBtn.addEventListener("click", (event) => {
  document.querySelectorAll(".grid-hidden").forEach(function (item) {
    item.classList.remove("grid-hidden");
  });

  loadMoreBtn.remove();
});

let tabImg = document.querySelector(".tabs-images");

tabImg.addEventListener("click", (event) => {
  if (event.target.classList.contains("tabs-title")) {
    document.querySelectorAll(".grid li").forEach(function (item) {
      item.classList.add("grid-hidden");
    });

    const dataTabValue = event.target.dataset.tab;
    document
      .querySelectorAll(`.grid li[data-tab=${dataTabValue}]`)
      .forEach(function (item) {
        item.classList.remove("grid-hidden");
      });
  }
});

var swiper = new Swiper(".mySwiper", {
  spaceBetween: 10,
  slidesPerView: 4,
  freeMode: true,
  watchSlidesProgress: true,
});
var swiper2 = new Swiper(".mySwiper2", {
  spaceBetween: 10,
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
  thumbs: {
    swiper: swiper,
  },
});
